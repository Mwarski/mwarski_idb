# -*- coding: utf-8 -*-
__author__ = 'ParesOne'
# DJANGO SESSION VIEWS

import smtplib
from django.shortcuts import render_to_response
from django.shortcuts import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from collections import Counter
import MySQLdb


def home_view(request):
    language = 'pl'
    session_language = 'gb'
    if 'lang' in request.COOKIES:
        language = request.COOKIES['lang']

    if 'lang' in request.session:
        session_language = request.session['lang']

    if request.user.is_authenticated():
        is_login = True
    else:
        is_login = False

    return render_to_response('index.html',
                              {'language': language,
                               'is_login': is_login,
                               'session_language': session_language})


def header(request):

    return render_to_response('header.html')


def task_list_view(request):
    if request.user.is_authenticated():
        is_login = True
    else:
        is_login = False

    username = request.user.username.encode("utf-8")
    task_list = []
    temp_list = []
    col_name = ['LP', 'NAZWA', 'GRUPA', 'OPIS', 'STATUS']

    #db = MySQLdb.connect()
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()
    # query = 'SELECT name, subject, description '
    # query += 'FROM task_list '
    # query += 'WHERE username = "%s" OR username = "all"' % username

    query = 'SELECT tl.name, tl.subject, tl.description, tls.is_done, tls.task_id, tls.is_delete '
    query += 'FROM task_list_status tls '
    query += 'INNER JOIN task_list tl '
    query += 'ON tls.task_id = tl.id '
    query += "WHERE tls.username = '" + username + "'"
    cursor.execute(query)
    list = cursor.fetchall()
    for row in list:
        if row[5] == 1:
            continue
        temp_list.append(row[0].encode("utf-8"))
        temp_list.append(row[1].encode("utf-8"))
        temp_list.append(row[2].encode("utf-8"))
        temp_list.append(row[3])
        temp_list.append(row[4])
        task_list.append(temp_list)
        temp_list = []
    db.close()

    return render_to_response('task_list.html',
                              {'list': task_list,
                               'col_name': col_name,
                               'is_login': is_login},  context_instance=RequestContext(request))

    #                          {"id_subjects": id_subjects,
    #                           "names": names,
    #                           "descriptions": desc})


def add_task_view(request):
    c = {}
    c.update(csrf(request))
    if request.user.is_authenticated():
        is_login = True
        group = []
        username = request.user.username.encode("utf-8")
        db = MySQLdb.connect(user='u1215762_root',
                             passwd='Quantumgis9#',
                             db='db1215762_idb',
                             host='mysql711.admin.strefa.pl',
                             charset='utf8')
        cursor = db.cursor()
        query = 'SELECT tl.subject '
        query += 'FROM task_list_status tls '
        query += 'INNER JOIN task_list tl '
        query += 'ON tls.task_id = tl.id '
        query += "WHERE tls.username = '" + username + "'"
        cursor.execute(query)
        list = cursor.fetchall()
        for row in list:
            group.append(row[0].encode("utf-8"))
        db.close()
        return render_to_response('add_task.html',
                                  {'range': range(1, 10),
                                   'group': group,
                                   'is_login': is_login}, context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect("/", context_instance=RequestContext(request))


def adding_task(request):

    task_name = request.POST.get('name').encode('utf-8')
    task_group = request.POST.get('group').encode('utf-8')
    task_description = request.POST.get('description').encode('utf-8')
    task_priority = request.POST.get('priority')
    username = request.user.username.encode("utf-8")
    #db = MySQLdb.connect()
    # db = MySQLdb.connect(user='root',
    #                      passwd='passroot',
    #                      db='idb',
    #                      host='localhost',
    #                      charset='utf8')
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()
    query = "INSERT INTO task_list (subject, name, description, username) "
    query += "VALUES ('"
    query += task_group + "','"
    query += task_name + "','"
    query += task_description + "','"
    query += username + "')"
    cursor.execute(query)
    try:
        db.commit()
    except:
        db.rollback()
    cursor = db.cursor()
    query = "SELECT id FROM task_list WHERE description='" + task_description + "'"
    query += " AND username='%s'" % username
    cursor.execute(query)
    task_id = cursor.fetchall()[0][0]
    query = "INSERT INTO task_list_status (username, task_id, is_done) "
    query += "VALUES ('"
    query += username + "','"
    query += str(task_id) + "',"
    query += "'0')"
    cursor.execute(query)
    try:
        db.commit()
    except:
        db.rollback()
    db.close()

    return HttpResponseRedirect("/task_list/")
    # return render_to_response("base.html",
    #                           {'is_login': True})


def saving_task(request):
    c = {}
    c.update(csrf(request))
    checked = request.POST.getlist('checked')
    task_id_list = request.POST.getlist('task_list_id')
    username = request.user.username.encode("utf-8")
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()
    print checked
    print task_id_list
    for utask_id in task_id_list:
        query = "UPDATE task_list_status "
        query += "SET is_done = 0 "
        query += "WHERE username = '" + username + "' "
        query += "AND task_id = " + utask_id
        cursor.execute(query)
        try:
            db.commit()
        except:
            db.rollback()

    for task_id in checked:
        query = "UPDATE task_list_status "
        query += "SET is_done = 1 "
        query += "WHERE username = '" + username + "' "
        query += "AND task_id = " + task_id
        cursor.execute(query)
        try:
            db.commit()
        except:
            db.rollback()

    db.close()

    return HttpResponseRedirect("/task_list/")
    # return render_to_response("base.html",
    #                           {'is_login': True})


def delete_task_view(request):
    if request.user.is_authenticated():
        is_login = True
    else:
        is_login = False

    username = request.user.username.encode("utf-8")
    task_list = []
    temp_list = []
    col_name = ['DEL', 'NAZWA', 'GRUPA', 'OPIS', 'STATUS']

    #db = MySQLdb.connect()
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()
    # query = 'SELECT name, subject, description '
    # query += 'FROM task_list '
    # query += 'WHERE username = "%s" OR username = "all"' % username

    query = 'SELECT tl.name, tl.subject, tl.description, tls.is_done, tls.task_id, is_delete '
    query += 'FROM task_list_status tls '
    query += 'INNER JOIN task_list tl '
    query += 'ON tls.task_id = tl.id '
    query += "WHERE tls.username = '" + username + "'"
    cursor.execute(query)
    list = cursor.fetchall()
    for row in list:
        if row[5] == True:
            continue
        temp_list.append(row[0].encode("utf-8"))
        temp_list.append(row[1].encode("utf-8"))
        temp_list.append(row[2].encode("utf-8"))
        temp_list.append(row[3])
        temp_list.append(row[4])
        task_list.append(temp_list)
        temp_list = []
    db.close()

    return render_to_response('delete_task.html',
                              {'list': task_list,
                               'col_name': col_name,
                               'is_login': is_login},  context_instance=RequestContext(request))

    #                          {"id_subjects": id_subjects,
    #                           "names": names,
    #                           "descriptions": desc})


def deleting_task(request):
    c = {}
    c.update(csrf(request))
    checked = request.POST.getlist('checked')
    delete_list = request.POST.getlist('delete_list')
    username = request.user.username.encode("utf-8")
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()
    for task_id in delete_list:
        query = "UPDATE task_list_status "
        query += "SET is_delete = 1 "
        query += "WHERE username = '" + username + "' "
        query += "AND task_id = " + task_id
        cursor.execute(query)
        try:
            db.commit()
        except:
            db.rollback()

    db.close()

    return HttpResponseRedirect("/task_list/")
    # return render_to_response("base.html",
    #                           {'is_login': True})


def language(request, language='pl'):

    response = HttpResponse('Setting language to %s' % language)
    response.set_cookie('lang', language)
    request.session['lang'] = language

    return response


def progress_view(request):
    if request.user.is_authenticated():
        is_login = True
    else:
        is_login = False

    username = request.user.username.encode("utf-8")
    task_list = []
    temp_list = []
    task_dict = {}
    col_name = ['LP', 'NAZWA', 'GRUPA', 'OPIS', 'STATUS']

    #db = MySQLdb.connect()
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()
    # query = 'SELECT name, subject, description '
    # query += 'FROM task_list '
    # query += 'WHERE username = "%s" OR username = "all"' % username

    query = 'SELECT tl.subject, tls.is_done, tls.task_id, tls.is_delete '
    query += 'FROM task_list_status tls '
    query += 'INNER JOIN task_list tl '
    query += 'ON tls.task_id = tl.id '
    query += "WHERE tls.username = '" + username + "'"
    cursor.execute(query)
    list = cursor.fetchall()
    print Counter(list)
    for row in list:
        if row[3] == 1:
            continue
        temp_list.append(row[0].encode("utf-8"))
        temp_list.append(row[1])
        temp_list.append(row[2])
        task_list.append(temp_list)
        temp_list = []
    db.close()
    return render_to_response('progress.html',
                              {'list': task_list,
                               'dict': task_dict,
                               'col_name': col_name,
                               'is_login': is_login},  context_instance=RequestContext(request))


def courses_view(request):
    if request.user.is_authenticated():
        is_login = True
    else:
        is_login = False
    #db = MySQLdb.connect()
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()
    cursor.execute('SELECT name, description, duration, price, status FROM courses_list')
    task_list = []
    temp_list = []
    col_name = ['LP', 'NAZWA KURSU', 'OPIS', 'CZAS', 'KOSZT', 'STATUS']
    for row in cursor.fetchall():
        temp_list.append(row[0].encode("utf-8"))
        temp_list.append(row[1].encode("utf-8"))
        temp_list.append(row[2])
        temp_list.append(row[3])
        temp_list.append(row[4])
        task_list.append(temp_list)
        temp_list = []
    db.close()

    return render_to_response('courses.html',
                              {'list': task_list,
                               'col_name': col_name,
                               'is_login': is_login})


def registration_view(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('registration.html', c)


def signing_up(request):
    username = request.POST.get('first_name', '')[0:2] + request.POST.get('last_name', '')
    firstname = request.POST.get('first_name', '')
    lastname = request.POST.get('last_name', '')
    password = request.POST.get('password', '')
    email = request.POST.get('email', '')
    #db = MySQLdb.connect()
    db = MySQLdb.connect(user='u1215762_root',
                         passwd='Quantumgis9#',
                         db='db1215762_idb',
                         host='mysql711.admin.strefa.pl',
                         charset='utf8')
    cursor = db.cursor()

    for task_id in range(20+1):
        if task_id == 0:
            continue
        query = "INSERT INTO task_list_status (username, task_id, is_done) VALUES ("
        query += "'" + username + "','" + str(task_id) + "', 0)"
        cursor.execute(query)
        try:
            db.commit()
        except:
            db.rollback()

    db.close()

    try:
        user = User.objects.create_user(username=username, first_name=firstname, last_name=lastname,
                                        password=password, email=email)
        user.save()
    except:
        print("USERNAME IS ALREADY EXISTS")
        return HttpResponseRedirect("/accounts/registration")

    return render_to_response('signed_up.html',
                              {'user': {'username': username,
                                        'first_name': firstname,
                                        'last_name': lastname}})


def login_view(request):
    c = {}
    c.update(csrf(request))
    if request.user.is_authenticated():
        is_login = True
        return render_to_response('base.html',
                                  {'is_login': is_login},
                                  c)

    return render_to_response('login.html', c)


def auth_view(request):
    # db = MySQLdb.connect(user='root', db='idb_users', passwd='', host='localhost')
    # cursor = db.cursor()
    # cursor.execute('SELECT name FROM users')
    # uname = [row[0] for row in cursor.fetchall()]
    # cursor.execute('SELECT password FROM users')
    # passw = [row[0] for row in cursor.fetchall()]
    # db.close()
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None:
        #if username in uname and password in passw:
        auth.login(request, user)

        return HttpResponseRedirect('/accounts/logged_in')
    else:
        return HttpResponseRedirect('/accounts/invalid')


def logged_in_view(request):

    return render_to_response('logged_in.html',
                              {'user': request.user,
                               'is_login': True})


def invalid_login_view(request):

    return render_to_response('invalid_login.html')


def logout_view(request):
    auth.logout(request)

    return render_to_response('logout.html')


def contact_view(request):
    c = {}
    c.update(csrf(request))
    if request.user.is_authenticated():
        is_login = True
        return render_to_response('contact.html',
                                  {'is_login': is_login},
                                  context_instance=RequestContext(request))

    return render_to_response('contact.html',
                              context_instance=RequestContext(request))


def contact_mail(request):
    sender = request.POST.get('sender')
    recivers = ['mwarski.zms@gmail.com']
    email = request.POST.get('e-mail')
    message = request.POST.get('message')

    try:
        smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
        smtpObj.sendmail(sender, recivers, message)
        print "SUCCESFULLY SENT EMAIL"
    except:
        print "ERROR: UNABLE TO SEND EMAIL"

    args = {'sender': sender,
            'recivers': recivers,
            'email': email,
            'message': message,
            'smtpObj': smtpObj}

    return render_to_response('sending.html',
                              {'args': args })