"""MichalW_IDB URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, patterns
from django.contrib import admin
from MichalW_IDB.views import language, auth_view, login_view, logged_in_view, logout_view, invalid_login_view, home_view, task_list_view, \
    progress_view, courses_view, contact_view, registration_view, signing_up, contact_mail, add_task_view, adding_task, saving_task, \
    delete_task_view, deleting_task

urlpatterns = patterns(
    '',
    url(r'^$', home_view),

    # tasks urls
    url(r'^task_list/$', task_list_view),
    url(r'^task_list/add_task/$', add_task_view),
    url(r'^task_list/add_task/adding$', adding_task),
    url(r'^task_list/saving_task/$', saving_task),
    url(r'^task_list/delete_task/$', delete_task_view),
    url(r'^task_list/deleting_task/$', deleting_task),

    url(r'^progress/$', progress_view),
    url(r'^courses/$', courses_view),
    url(r'^contact/$', contact_view),
    url(r'^admin/', admin.site.urls),
    url(r'^language/(?P<language>[a-z\-]+)/$', language),


    # user auth urls
    url(r'^accounts/login/$', login_view),
    url(r'^accounts/auth/$', auth_view),
    url(r'^accounts/logout/$', logout_view),
    url(r'^accounts/logged_in/$', logged_in_view),
    url(r'^accounts/invalid/$', invalid_login_view),
    url(r'^accounts/registration/$', registration_view),
    url(r'^accounts/signing_up/$', signing_up),

    # mail contact urls
    url(r'^mails/contact/sending$', contact_mail),

) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
